package br.lds;

import java.util.Arrays;

/**
 * Hello world!
 *
 */
public class App 
{
	String nome;
	Dependencia dep;
	
	App(String nome) 
	{
		this.nome = nome;
	}

	App(Dependencia dep) 
	{
		this.dep = dep;
	}

    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
    
    public void ordenaNome() 
    {
    	char[] nomeArray = nome.toCharArray();
    	Arrays.sort(nomeArray);
    	nome = String.valueOf(nomeArray);
    }
    
    public int operacaoComDependencia(String param) {
    	int result = 2 * dep.operacaoDaDependencia(param);
    	return result;
    }
}
