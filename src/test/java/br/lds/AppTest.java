package br.lds;

import static org.mockito.Mockito.*;

import org.mockito.Mock;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
	
	private App app;
	 
	@Mock
	private Dependencia dependencia;
	  
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
    public void testResult()
    {
    	// Arrange
    	app = new App("jose");
    	// Act
    	app.ordenaNome();
    	// Assert
    	assertEquals("ejos", app.nome);
    }
    
    public void testMockDeDependencia() 
    {
    	// Arrange
    	dependencia = mock(Dependencia.class);
    	app = new App(dependencia);
    	when(app.operacaoComDependencia("10")).thenReturn(10);
    	// Act
    	int result = app.operacaoComDependencia("10");
    	// Assert
    	assertEquals(20, result);
    }
}
